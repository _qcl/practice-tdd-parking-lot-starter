package com.parkinglot;

import java.util.HashSet;
import java.util.Set;

public class ParkingLot {

    private Set<Car> carSet;

    public Integer getCapacity() {
        return capacity;
    }

    private Integer capacity;

    public ParkingLot() {
        this.carSet = new HashSet<>();
        this.capacity = 10;
    }
    public ParkingLot(int capacity) {
        this.carSet = new HashSet<>();
        this.capacity = capacity;
    }
    public Ticket park(Car car) {
        if(isFull()){
            throw new NoAvailablePositionException();
        }
        if(!carSet.contains(car)){
            carSet.add(car);
        } else {
            return null;
        }
        return new Ticket(car);
    }

    public Car fetch(Ticket ticket) {
        if(ticket == null || ticket.getCar() == null){
            throw new UnrecognizedTicketException();
        }
        if(!isCarInHere(ticket.getCar())){
            throw new UnrecognizedTicketException();
        }
        carSet.remove(ticket.getCar());
        return ticket.getCar();
    }
    public boolean isCarInHere(Car car){
        return this.carSet.contains(car);
    }
    public boolean isFull(){
        return this.carSet.size() >= this.capacity;
    }

    public int getVacancy() {
        return this.capacity - carSet.size();
    }
}
