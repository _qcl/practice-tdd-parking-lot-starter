package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SuperSmartParkingBoyTest {
    @Test
    void should_car_in_parkingLot_has_larger_availablePositionRate_when_SuperSmartParkingBoy_park_given_superSuperSmartParkingBoy_car_two_parking_lot() {
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(100);
        ParkingLot moreEmptyParkingLot = new ParkingLot(10);
        parkingLot.park(new Car());
        parkingLots.add(parkingLot);
        parkingLots.add(moreEmptyParkingLot);
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car = new Car();

        parkingBoy.parkCar(car);

        Assertions.assertTrue(moreEmptyParkingLot.isCarInHere(car));
    }
    @Test
    void should_return_ticket_when_park_car_given_a_car_and_parking_lot_and_SuperSmartParkingBoy() {
        ParkingLot parkingLot = new ParkingLot();
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot);
        Car car = new Car();

        Ticket ticket = parkingBoy.parkCar(car);

        Assertions.assertNotNull(ticket);
    }
    @Test
    void should_return_a_car_when_fetch_car_given_a_ticket_and_a_parking_lot_parked_car_and_SuperSmartParkingBoy() {
        ParkingLot parkingLot = new ParkingLot();
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot);
        Car car = new Car();
        Ticket ticket = parkingBoy.parkCar(car);

        Car parkedCar = parkingBoy.fetch(ticket);

        Assertions.assertSame(car, parkedCar);
    }
    @Test
    void should_return_right_cars_when_fetch_car_twice_given_ticket_and_parking_lot_and_SuperSmartParkingBoy() {
        ParkingLot parkingLot = new ParkingLot();
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingBoy.parkCar(car1);
        Ticket ticket2 = parkingBoy.parkCar(car2);

        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);

        Assertions.assertSame(car1, fetchedCar1);
        Assertions.assertSame(car2, fetchedCar2);
    }
    @Test
    void should_throw_exception_when_fetch_car_given_a_wrong_ticket_and_parking_lot_and_SuperSmartParkingBoy() {
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Ticket ticket = new Ticket();

        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,() -> parkingBoy.fetch(ticket));
        Assertions.assertSame("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }
    @Test
    void should_return_throw_exception_when_fetch_car_given_a_used_ticket_and_parking_lot_and_SuperSmartParkingBoy() {
        ParkingLot parkingLot = new ParkingLot();
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);

        parkingLot.fetch(ticket);
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,() -> parkingBoy.fetch(ticket));
        Assertions.assertSame("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }
    @Test
    void should_return_null_when_park_car_given_a_parking_lot_is_full_and_SuperSmartParkingBoy() {
        ParkingLot parkingLot = new ParkingLot(10);
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot);
        Car car = new Car();
        for (int i = 0;i < 10;i ++){
            parkingLot.park(new Car());
        }
        NoAvailablePositionException noAvailablePositionException =
                Assertions.assertThrows(NoAvailablePositionException.class,() -> parkingBoy.parkCar(car));
        Assertions.assertSame("No available position.", noAvailablePositionException.getMessage());
    }
    @Test
    void should_park_car_in_first_parking_lot_when_parkingBoy_park_car_given_two_parking_lot_and_car_and_SuperSmartParkingBoy() {
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(10);
        parkingLots.add(parkingLot);
        parkingLots.add(new ParkingLot(10));
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car = new Car();

        parkingBoy.parkCar(car);

        Assertions.assertTrue(parkingLot.isCarInHere(car));
    }
    @Test
    void should_park_car_in_second_parking_lot_when_parkingBoy_park_car_given_car_and_SuperSmartParkingBoy_and_one_empty_parking_lot_and_a_full_parking_lot() {
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot emptyParkingLot = new ParkingLot(10);
        ParkingLot fullParkingLot = new ParkingLot(10);
        for (int i = 0;i < 10;i ++){
            fullParkingLot.park(new Car());
        }
        parkingLots.add(fullParkingLot);
        parkingLots.add(emptyParkingLot);
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car = new Car();
        parkingBoy.parkCar(car);
        Assertions.assertFalse(fullParkingLot.isCarInHere(car));
        Assertions.assertTrue(emptyParkingLot.isCarInHere(car));
    }
    @Test
    void return_the_right_car_with_each_ticket_when_parkingBoy_fetch_car_given_two_car_and_SuperSmartParkingBoy_and_two_parking_lot() {
        List<ParkingLot> parkingLots = new ArrayList<>();
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1= new ParkingLot(10);
        ParkingLot parkingLot2= new ParkingLot(10);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLots);
        Ticket ticket1 = parkingBoy.parkCar(car1);
        Ticket ticket2 = parkingBoy.parkCar(car2);
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);
        Assertions.assertSame(fetchedCar1, car1);
        Assertions.assertSame(fetchedCar2, car2);
    }
    @Test
    void should_return_throw_exception_when_fetch_car_given_a_SuperSmartParkingBoy_and_a_unrecognized_ticket_and_two_parking_lot() {
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(10));
        parkingLots.add(new ParkingLot(10));
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLots);

        Ticket ticket = new Ticket();

        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,() -> parkingBoy.fetch(ticket));
        Assertions.assertSame("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }
    @Test
    void should_return_throw_exception_when_fetch_car_given_a_SuperSmartParkingBoy_and_a_used_ticket_and_two_parking_lot() {
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(new ParkingLot(10));
        parkingLots.add(new ParkingLot(10));
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLots);

        Ticket ticket = new Ticket();

        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,() -> parkingBoy.fetch(ticket));
        Assertions.assertSame("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }
    @Test
    void should_return_exception_when_parkingBoy_park_car_given_car_and_SuperSmartParkingBoy_and_two_full_parking_lot() {
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot fullParkingLot1 = new ParkingLot(10);
        ParkingLot fullParkingLot2 = new ParkingLot(10);
        for (int i = 0;i < 10;i ++){
            fullParkingLot1.park(new Car());
            fullParkingLot2.park(new Car());
        }
        parkingLots.add(fullParkingLot1);
        parkingLots.add(fullParkingLot2);
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car = new Car();


        NoAvailablePositionException noAvailablePositionException =
                Assertions.assertThrows(NoAvailablePositionException.class, () -> parkingBoy.parkCar(car));
        Assertions.assertSame("No available position.", noAvailablePositionException.getMessage());
    }
}
