package com.parkinglot;

public class NoAvailablePositionException extends RuntimeException {
    public String getMessage(){
        return "No available position.";
    }
}
