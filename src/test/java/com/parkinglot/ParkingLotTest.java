package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_car_given_a_car_and_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        Ticket ticket = parkingLot.park(car);

        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_fetch_car_given_a_ticket_and_a_parking_lot_parked_car() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);

        Car parkedCar = parkingLot.fetch(ticket);

        Assertions.assertSame(car, parkedCar);
    }

    @Test
    void should_return_right_cars_when_fetch_car_twice_given_ticket_and_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        Assertions.assertSame(car1, fetchedCar1);
        Assertions.assertSame(car2, fetchedCar2);
    }

    @Test
    void should_return_null_when_fetch_car_given_a_wrong_ticket_and_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = new Ticket();

        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,() -> parkingLot.fetch(ticket));
        Assertions.assertSame("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_null_when_fetch_car_given_a_used_ticket_and_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);

        parkingLot.fetch(ticket);
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,() -> parkingLot.fetch(ticket));
        Assertions.assertSame("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_null_when_park_car_given_a_parking_lot_is_full() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        for (int i = 0;i < 10;i ++){
            parkingLot.park(new Car());
        }
        NoAvailablePositionException noAvailablePositionException =
                Assertions.assertThrows(NoAvailablePositionException.class,() -> parkingLot.park(car));
        Assertions.assertSame("No available position.", noAvailablePositionException.getMessage());
    }
}
