package com.parkinglot;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.function.Predicate.not;

public class SmartParkingBoy extends ParkingBoy {

    public SmartParkingBoy() {
        super();
    }
    public SmartParkingBoy(ParkingLot parkingLot) {
        super(parkingLot);
    }
    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    @Override
    public Ticket parkCar(Car car) {
        VacancyComparator vacancyComparator = new VacancyComparator();
        return this.parkingLots.stream().max(vacancyComparator).
                map(lot -> lot.park(car)).orElseThrow(NoAvailablePositionException::new);
    }



}
