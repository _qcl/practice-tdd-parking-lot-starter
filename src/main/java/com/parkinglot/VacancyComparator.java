package com.parkinglot;

import java.util.Comparator;

public class VacancyComparator implements Comparator<ParkingLot> {
    @Override
    public int compare(ParkingLot o1, ParkingLot o2) {
        return o1.getVacancy() - o2.getVacancy();
    }
}
