package com.parkinglot;

import java.math.BigDecimal;
import java.util.Comparator;

public class VacancyRateComparator implements Comparator<ParkingLot> {
    @Override
    public int compare(ParkingLot o1, ParkingLot o2) {
        BigDecimal bigDecimal1 = new BigDecimal((o1.getVacancy() * 1.0) / o1.getCapacity());
        BigDecimal bigDecimal2 = new BigDecimal((o2.getVacancy() * 1.0) / o2.getCapacity());

        return bigDecimal1.compareTo(bigDecimal2);
    }
}
