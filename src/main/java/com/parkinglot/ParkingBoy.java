package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

import static java.util.function.Predicate.not;

public class ParkingBoy {

    protected List<ParkingLot> parkingLots;

    public ParkingBoy() {
        this.parkingLots = new ArrayList<>();
    }

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLots = new ArrayList<>();
        this.parkingLots.add(parkingLot);
    }
    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket parkCar(Car car) {
        return parkingLots.stream().filter(not(ParkingLot::isFull)).findFirst().
                map(lot -> lot.park(car)).orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        return this.parkingLots.stream().filter(lot -> lot.isCarInHere(ticket.getCar())).
                findAny().map(parkingLot -> parkingLot.fetch(ticket)).
                orElseThrow(UnrecognizedTicketException::new);
    }
}
