package com.parkinglot;

import java.util.List;

public class SuperSmartParkingBoy extends ParkingBoy{

    public SuperSmartParkingBoy() {
        super();
    }
    public SuperSmartParkingBoy(ParkingLot parkingLot) {
        super(parkingLot);
    }
    public SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    @Override
    public Ticket parkCar(Car car) {
        VacancyRateComparator vacancyComparator = new VacancyRateComparator();
        return this.parkingLots.stream().max(vacancyComparator).
                map(lot -> lot.park(car)).orElseThrow(NoAvailablePositionException::new);
    }
}
