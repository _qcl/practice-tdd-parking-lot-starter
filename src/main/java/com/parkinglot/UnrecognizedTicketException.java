package com.parkinglot;

public class UnrecognizedTicketException extends RuntimeException {
    public String getMessage(){
        return "Unrecognized parking ticket.";
    }
}
